package jp.yfujiwara.stone

import jp.yfujiwara.stone.ast.ASTree
import jp.yfujiwara.stone.ast.ASTList
import kotlin.reflect.KClass

internal abstract class Factory {
    internal abstract fun make0(arg: Any): ASTree

    /**
     * 可視性をパッケージ内にしたかったのでinternalとした
     * ASTを作るメソッド
     * @param arg Any
     * @return ASTree
     */
    internal fun make(arg: Any): ASTree {
        try {
            return make0(arg)
        } catch (e1: IllegalArgumentException) {
            throw e1
        } catch (e2: Exception) {
            // コンパイラが壊れている場合
            throw RuntimeException(e2)
        }
    }

    companion object {
        /**
         * Kotlinのprotectedは本当に継承関係の間でしか可視性が担保されない
         * 同じパッケージ内に公開するなら近いものはinternalしかない
         * @param clazz KClass<out ASTree>?
         * @return Factory
         */
        @JvmStatic
        internal fun getForASTList(clazz: KClass<out ASTree>?): Factory {
            var f = get(clazz, List::class)
            if (f == null) {
                f = object : Factory() {
                    internal override fun make0(arg: Any): ASTree {
                        if (arg is List<*>) {
                            // ワイルドカードのリストにキャストしてから型に当てはまるものだけを抽出
                            val results: List<ASTree> = arg.filterIsInstance<ASTree>()
                            return if (results.size == 1) {
                                results[0]
                            } else {
                                ASTList(results)
                            }
                        } else {
                            throw ClassCastException("cannot cast ${arg.javaClass.kotlin} to List<ASTree>.")
                        }
                    }
                }
            }
            return f
        }

        /**
         * こちらも単なるprotected staticメソッドなのでinternalとする
         * @param clazz KClass<out ASTree>?
         * @param argType KClass<*>
         * @return Factory?
         */
        @JvmStatic
        internal fun get(clazz: KClass<out ASTree>?, argType: KClass<*>): Factory? {
            if (clazz == null) {
                return null
            }

            try {
                // Javaのコード呼び出し時の戻り値の最後には！がつく
                // finalはvalにしておけば良い
                // createメソッドがある場合はコンストラクタに変わり対象メソッドを利用するためにメソッドを取得
                val m = clazz.java.getMethod(factoryName, argType.java)
                return object : Factory() {
                    internal override fun make0(arg: Any): ASTree {
                        val ret = m.invoke(null, arg)
                        if (ret is ASTree) {
                            return ret
                        } else {
                            throw ClassCastException("cannot cast ${ret.javaClass.kotlin} to ASTree.")
                        }
                    }
                }
            } catch (e: NoSuchMethodException) {
                // createメソッドがない場合はコンストラクタでのインスタンス生成を行うのでここのエラーは握りつぶす
            }

            try {
                // argTypeの引数を持つコンストラクタメソッドを取得
                val c = clazz.java.getConstructor(argType.java)
                return object : Factory() {
                    internal override fun make0(arg: Any): ASTree {
                        // 作ったコンストラクタからインスタンスを生成する
                        return c.newInstance(arg)
                    }
                }
            } catch (e: NoSuchMethodException) {
                throw RuntimeException(e)
            }
        }
    }
}