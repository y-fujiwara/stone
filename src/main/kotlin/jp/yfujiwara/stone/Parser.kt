package jp.yfujiwara.stone

import jp.yfujiwara.stone.ast.ASTLeaf
import jp.yfujiwara.stone.ast.ASTList
import jp.yfujiwara.stone.ast.ASTree
import kotlin.reflect.KClass

/**
 * LL(1)構文解析器を組み立てるパーサコンビネータクラス
 */
class Parser {
    /**
     * レールロードダイアグラムのいづれかの要素を示す抽象クラス
     */
    internal abstract class Element {
        /**
         * 対象の種類の要素に従ってパースを行うメソッド
         * @param lexer Lexer
         * @param res MutableList<ASTree> TODO: これをListに変えて戻り値をListにすること
         */
        internal abstract fun parse(lexer: Lexer, res: MutableList<ASTree>)

        /**
         * 次に対象の要素が出てくるか先読みするメソッド
         * @param lexer Lexer
         * @return Boolean
         */
        internal abstract fun match(lexer: Lexer): Boolean
    }

    /**
     * レールロードダイアグラムの式などの木構造になっている部分を示すクラス
     * @property parser Parser
     * @constructor
     */
    internal class Tree(protected val parser: Parser) : Element() {
        /**
         * 式などの木構造をパースするメソッド
         * @param lexer Lexer
         * @param res MutableList<ASTree>
         */
        internal override fun parse(lexer: Lexer, res: MutableList<ASTree>) {
            res.add(parser.parse(lexer))
        }

        /**
         * マッチする要素があるか確かめるメソッド
         * @param lexer Lexer
         * @return Boolean
         */
        internal override fun match(lexer: Lexer): Boolean {
            return parser.match(lexer)
        }
    }

    /**
     * レールロードダイアグラムの分岐部分を示すクラス
     * @property parsers Array<Parser?>
     * @constructor
     */
    internal class OrTree(private var parsers: Array<Parser>) : Element() {
        /**
         * 分岐先に選んだ種類の要素をパースするメソッド
         * 基本は2個まで。chooseで対象のパーサが選ばれる
         * @param lexer Lexer
         * @param res MutableList<ASTree>
         */
        override fun parse(lexer: Lexer, res: MutableList<ASTree>) {
            val p = choose(lexer)
            if (p == null) {
                throw ParseException(lexer.peek(NEXT))
            } else {
                res.add(p.parse(lexer))
            }
        }

        /**
         * 対象のパーサが存在するか確かめるメソッド
         * @param lexer Lexer
         * @return Boolean
         */
        override fun match(lexer: Lexer): Boolean {
            return choose(lexer) != null
        }

        /**
         * パーサ配列の中に対象のトークンをバースできるパーサを取得するメソッド
         * @param lexer Lexer
         * @return Parser?
         */
        private fun choose(lexer: Lexer): Parser? {
            for (p in parsers) {
                if (p.match(lexer)) {
                    return p
                }
            }
            return null
        }

        /**
         * パーサ配列の先頭に新たなパーサを追加するメソッド
         * @param p Parser
         */
        internal fun insert(p: Parser) {
            // 配列の先頭にpを付け加えた新たな配列を生成する
            parsers = arrayOf(p, *parsers)
        }
    }

    /**
     * レールロードダイアグラムの繰り返し部分を表現するクラス
     * @property parser Parser
     * @property onlyOnce Boolean
     * @constructor
     */
    internal class Repeat(private val parser: Parser, private val onlyOnce: Boolean) : Element() {
        /**
         * 繰り返しなので対象の要素にマッチし続ける限りパースを行い続けるメソッド
         * @param lexer Lexer
         * @param res MutableList<ASTree>
         */
        internal override fun parse(lexer: Lexer, res: MutableList<ASTree>) {
            while (parser.match(lexer)) {
                val t: ASTree = parser.parse(lexer)
                if (t.javaClass.kotlin != ASTList::class || t.numChildren() > 0) {
                    res.add(t)
                }
                if (onlyOnce) {
                    break
                }
            }
        }

        /**
         * マッチするパーサか判別するメソッド
         * @param lexer Lexer
         * @return Boolean
         */
        internal override fun match(lexer: Lexer): Boolean {
            return parser.match(lexer)
        }
    }


    /**
     * KClassの型引数Tは戻り値にしか出てこないのでout(共変)で良い
     * レールロードダイアグラム上の何かしらのトークンを示す抽象クラス
     */
    internal abstract class AToken(type: KClass<out ASTLeaf>?) : Element() {
        /**
         * 各種トークンクラスのインスタンス生成用ファクトリ
         */
        private val factory = if (type == null) {
            Factory.get(ASTLeaf::class, Token::class)
        } else {
            Factory.get(type, Token::class)
        }

        internal abstract fun test(t: Token): Boolean

        /**
         * パースを行い何かトークンを生成するメソッド
         * @param lexer Lexer
         * @param res MutableList<ASTree>
         */
        internal override fun parse(lexer: Lexer, res: MutableList<ASTree>) {
            val t = lexer.read()
            if (test(t) && factory != null) {
                val leaf = factory.make(t)
                res.add(leaf)
            } else {
                throw ParseException(t)
            }
        }

        /**
         * 取得したいトークンにマッチするか調べるメソッド
         * @param lexer Lexer
         * @return Boolean
         */
        internal override fun match(lexer: Lexer): Boolean {
            return test(lexer.peek(NEXT))
        }
    }

    /**
     * 対象のトークンが識別子か確かめるクラス
     */
    internal class IdToken(type: KClass<out ASTLeaf>?, r: HashSet<String>?) : AToken(type) {
        private val reserved = r ?: HashSet<String>()

        internal override fun test(t: Token): Boolean = t.isIdentifier() && !reserved.contains(t.getText())
    }

    /**
     * 対象のトークンが数字か確かめるクラス
     */
    internal class NumToken(type: KClass<out ASTLeaf>?) : AToken(type) {
        internal override fun test(t: Token): Boolean = t.isNumber()
    }

    /**
     * 対象のトークンが文字列か確かめるクラス
     */
    internal class StrToken(type: KClass<out ASTLeaf>?) : AToken(type) {
        internal override fun test(t: Token): Boolean = t.isString()
    }


    /**
     * 何かしらのトークンで表現される木構造の葉を表現するクラス
     */
    internal open class Leaf(private val tokens: Array<String>) : Element() {

        /**
         * パーサの中で対象のトークンが見つかった場合に要素の配列に追加するメソッド
         */
        override fun parse(lexer: Lexer, res: MutableList<ASTree>) {
            val t = lexer.read()
            if (t.isIdentifier()) {
                for (token in tokens) {
                    if (token == t.getText()) {
                        find(res, t)
                        return
                    }
                }
            }

            // ここまで来てトークンが空じゃない = マッチするトークンがなかった
            if (tokens.isNotEmpty()) {
                throw ParseException("${tokens[0]} expected. $t")
            } else {
                // 空の場合はそもそもトークンが存在しない
                throw ParseException(t)
            }
        }

        /**
         * 対象となっているトークンが一つでも存在するか探すメソッド
         */
        override fun match(lexer: Lexer): Boolean {
            val t = lexer.peek(NEXT)
            if (t.isIdentifier()) {
                for (token in tokens) {
                    if (token == t.getText()) {
                        return true
                    }
                }
            }
            return false
        }

        protected open fun find(res: MutableList<ASTree>, t: Token) {
            res.add(ASTLeaf(t))
        }
    }

    /**
     * Skip対象のトークンを示すクラス
     * TODO: Leafを継承しているが使われ方によっては委譲にしたほうが良いかも(by Leafみたいな)
     */
    internal class Skip(s: Array<String>) : Leaf(s) {
        protected override fun find(res: MutableList<ASTree>, t: Token) {}
    }

    /**
     * 演算子の順位と結合要件を保持するためのクラス
     */
    class Precedence(internal val value: Int, internal val leftAssoc: Boolean) {
    }

    /**
     * 任意個の演算子とその順位を保持できるようにしたクラス
     */
    class Operators : HashMap<String, Precedence>() {
        companion object {
            var LEFT = true
            var RIGHT = false
        }

        fun add(name: String, prec: Int, leftAssoc: Boolean) {
            put(name, Precedence(prec, leftAssoc))
        }
    }

    /**
     * レールロードダイアグラム上で何かしらの式要素を表すクラス
     */
    internal class Expr(clazz: KClass<out ASTree>?, private val factor: Parser, private val ops: Operators) :
        Element() {
        /**
         * 式をトークンにパースするファクトリ
         */
        private val factory = Factory.getForASTList(clazz)

        /**
         * 式なのでASTreeを継承したクラスリストにパースする
         */
        override fun parse(lexer: Lexer, res: MutableList<ASTree>) {
            var right = factor.parse(lexer)
            var prec = nextOperator(lexer)

            while (prec != null) {
                right = doShift(lexer, right, prec.value)
                prec = nextOperator(lexer)
            }

            res.add(right)
        }

        override fun match(lexer: Lexer): Boolean {
            return factor.match(lexer)
        }

        /**
         * 演算子の順位に従いリストを詰め込むメソッド
         */
        private fun doShift(lexer: Lexer, left: ASTree, prec: Int): ASTree {
            val list = mutableListOf<ASTree>()
            list.add(left)
            list.add(ASTLeaf(lexer.read()))
            var right = factor.parse(lexer)

            var next = nextOperator(lexer)
            while ((next != null) && rightIsExpr(prec, next)) {
                right = doShift(lexer, right, next.value)
                next = nextOperator(lexer)
            }
            list.add(right)
            return factory.make(list)
        }

        /**
         * 演算子順位法による次の演算子を先読みするメソッド
         */
        private fun nextOperator(lexer: Lexer): Precedence? {
            val t = lexer.peek(NEXT)
            return if (t.isIdentifier()) {
                ops[t.getText()]
            } else {
                null
            }
        }

        /**
         * 演算子の右辺が式か判別するメソッド
         */
        private fun rightIsExpr(prec: Int, nextPrec: Precedence): Boolean {
            return if (nextPrec.leftAssoc) {
                prec < nextPrec.value
            } else {
                prec <= nextPrec.value
            }
        }
    }

    companion object {
        /**
         * 対象抽象構文木のトークンに対するルールを定義するメソッド
         * @param clazz KClass<out ASTLeaf>?
         * @return Parser
         */
        fun rule(clazz: KClass<out ASTree>?) = Parser(clazz)

        fun rule(): Parser = rule(null)
    }

    /**
     * コンストラクタの中で呼ばれる初期化メソッドによって初期化されるメンバ変数たち
     */
    internal lateinit var elements: MutableList<Element>
    internal lateinit var factory: Factory

    constructor(clazz: KClass<out ASTree>?) {
        reset(clazz)
    }

    internal constructor(p: Parser) {
        elements = p.elements
        factory = p.factory
    }

    /**
     * 文法規則を空にするメソッド
     * @return Parser
     */
    fun reset(): Parser {
        elements = mutableListOf()
        return this
    }

    /**
     * 文法規則を空にしつつ節のクラスをcにする
     * @param clazz KClass<out ASTLeaf>?
     * @return Parser
     */
    fun reset(clazz: KClass<out ASTree>?): Parser {
        elements = mutableListOf()
        factory = Factory.getForASTList(clazz)
        return this
    }

    /**
     * パースを実行し、抽象構文木のトークンを一つ取得するメソッド
     */
    fun parse(lexer: Lexer): ASTree {
        val results = mutableListOf<ASTree>()
        for (e in elements) {
            e.parse(lexer, results)
        }
        return factory.make(results)
    }

    /**
     * 引数で与えられたものについてマッチするものが存在するか確かめるメソッド
     * @param lexer Lexer
     * @return Boolean
     */
    internal fun match(lexer: Lexer): Boolean {
        return if (elements.isEmpty()) {
            true
        } else {
            val e = elements[0]
            e.match(lexer)
        }
    }

    /**
     * BNFでの数字に当たる終端記号を表現するメソッド
     * @return Parser
     */
    fun number(): Parser = number(null)

    /**
     * BNFでの数字に当たる終端記号を表現するメソッド
     * @param clazz KClass<out ASTLeaf>?
     * @return Parser
     */
    fun number(clazz: KClass<out ASTLeaf>?): Parser {
        elements.add(NumToken(clazz))
        return this
    }

    /**
     * BNFでの予約後rを除く識別子(変数名とか)に当たる部分を表現するメソッド
     * @param reserved HashSet<String> 予約語や記号などの変数名に使えない文字　
     * @return Parser
     */
    fun identifier(reserved: HashSet<String>): Parser = identifier(null, reserved)

    /**
     * BNFでの予約後rを除く識別子(変数名とか)に当たる部分を表現するメソッド
     * @param clazz KClass<out ASTLeaf>?
     * @param reserved HashSet<String> 予約語や記号などの変数名に使えない文字　
     * @return Parser
     */
    fun identifier(clazz: KClass<out ASTLeaf>?, reserved: HashSet<String>): Parser {
        elements.add(IdToken(clazz, reserved))
        return this
    }

    /**
     * BNFでの文字列に当たる部分を表現するメソッド
     * @return Parser
     */
    fun string(): Parser = string(null)

    /**
     * BNFでの文字列に当たる部分を表現するメソッド
     * @param clazz KClass<out ASTLeaf>?
     * @return Parser
     */
    fun string(clazz: KClass<out ASTLeaf>?): Parser {
        elements.add(StrToken(clazz))
        return this
    }

    /**
     * BNFでの入力されたパターンに合致する識別子を表現するメソッド
     * @param pat Array<out String>
     * @return Parser
     */
    fun token(vararg pat: String): Parser {
        // 配列引数の前にアスタリスクつけると展開されるのでそれを改めてarrayにする
        elements.add(Leaf(arrayOf(*pat)))
        return this
    }

    /**
     * 抽象構文木に含めない終端記号かつ入力されたパターンに合致する識別子を表現するメソッド
     * @param pat Array<out String>
     * @return Parser
     */
    fun sep(vararg pat: String): Parser {
        elements.add(Skip(arrayOf(*pat)))
        return this
    }

    /**
     * 非終端記号pを表現するメソッド
     * @param p Parser
     * @return Parser
     */
    fun ast(p: Parser): Parser {
        elements.add(Tree(p))
        return this
    }

    /**
     * 非終端記号p1,p2,...のorを表現するメソッド
     * @param p Array<out Parser>
     * @return Parser
     */
    fun or(vararg p: Parser): Parser {
        elements.add(OrTree(arrayOf(*p)))
        return this
    }

    /**
     * 省略可能な非終端記号pを表現するメソッド(省略時には根だけのASTとする)
     * @param p Parser
     * @return Parser
     */
    fun maybe(p: Parser): Parser {
        val p2 = Parser(p)
        p2.reset()
        elements.add(OrTree(arrayOf(p, p2)))
        return this
    }

    /**
     * 省略可能な非終端記号pを表現するメソッド
     * @param p Parser
     * @return Parser
     */
    fun option(p: Parser): Parser {
        elements.add(Repeat(p, true))
        return this
    }

    /**
     * 非終端記号pの0回異常の繰り返しを表現するメソッド　
     * @param p Parser
     * @return Parser
     */
    fun repeat(p: Parser): Parser {
        elements.add(Repeat(p, false))
        return this
    }

    /**
     * BNFの二項演算式を表現するメソッド
     * @param subexp Parser
     * @param operators Operators
     * @return Parser
     */
    fun expression(subexp: Parser, operators: Operators): Parser {
        elements.add(Expr(null, subexp, operators))
        return this
    }

    /**
     * BNFの二項演算式を表現するメソッド
     * @param clazz KClass<out ASTLeaf>?
     * @param subexp Parser
     * @param operators Operators
     * @return Parser
     */
    fun expression(clazz: KClass<out ASTree>?, subexp: Parser, operators: Operators): Parser {
        elements.add(Expr(clazz, subexp, operators))
        return this
    }

    /**
     * 分布規則の先頭のorに新たな選択肢を挿入する
     * @param p Parser
     * @return Parser
     */
    fun insertChoice(p: Parser): Parser {
        val e = elements.first()
        if (e is OrTree) {
            e.insert(p)
        } else {
            val otherwise = Parser(this)
            reset(null)
            or(p, otherwise)
        }
        return this
    }
}