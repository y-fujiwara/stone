package jp.yfujiwara.stone

import jp.yfujiwara.stone.Parser.Operators
import jp.yfujiwara.stone.Parser.Operators.Companion.LEFT
import jp.yfujiwara.stone.Parser.Operators.Companion.RIGHT
import jp.yfujiwara.stone.Parser.Companion.rule
import jp.yfujiwara.stone.ast.*

class BasicParser() {
    /**
     * 予約語や記号などの変数名に使えない文字　
     */
    private val reserved = HashSet<String>()
    private val operators = Operators()

    /**
     * 定義の循環を表現するためのメンバ
     * expr -> factor -> primary -> expr
     */
    private val expr0 = rule()


    private val primary = rule(PrimaryExpr::class).or(
        rule().sep("(").ast(expr0).sep(")"),
        rule().number(NumberLiteral::class),
        rule().identifier(Name::class, reserved),
        rule().string(StringLiteral::class)
    )

    private val factor = rule().or(
        rule(NegativeExpr::class).sep("-").ast(primary),
        primary
    )

    private val expr = expr0.expression(BinaryExpr::class, factor, operators)

    private val statement0 = rule()

    private val block = rule(BlockStmnt::class)
        .sep("{").option(statement0)
        .repeat(rule().sep(";", EOL).option(statement0))
        .sep("}")

    private val simple = rule(PrimaryExpr::class).ast(expr)

    private val statement = statement0.or(
        rule(IfStmnt::class).sep("if").ast(expr).ast(block).option(
            rule().sep("else").ast(block)
        ),
        rule(WhileStmnt::class).sep("while").ast(expr).ast(block),
        simple
    )

    private val program = rule().or(statement, rule(NullStmnt::class)).sep(";", EOL)

    init {
        reserved.add(";")
        reserved.add("}")
        reserved.add(EOL)

        operators.add("=", 1, RIGHT)
        operators.add("==", 2, LEFT)
        operators.add(">", 2, LEFT)
        operators.add("<", 2, LEFT)
        operators.add("+", 3, LEFT)
        operators.add("-", 3, LEFT)
        operators.add("*", 4, LEFT)
        operators.add("/", 4, LEFT)
        operators.add("%", 4, LEFT)
    }

    fun parse(lexer: Lexer): ASTree = program.parse(lexer)
}