package jp.yfujiwara.stone

/**
 * この定数については元のJavaについても匿名サブクラスを作っていたっぽい
 * 今回はトップレベル変数にする。valが再代入不可なのでそれだけでもいいかも
 * コンストラクタ呼び出しにnewもいらない
 * object自体は匿名クラスを作ったりする仕組み
 */
@JvmField
val EOF = object : Token(-1) {}

/**
 * 一行の端に当たる改行コード
 */
const val EOL = "\\n"

const val TRUE = 1
const val FALSE = 0

/**
 * トークンパース用正規表現
 */
const val regexPat = "\\s*((//.*)|([0-9]+)|(\"(\\\\\"|\\\\\\\\|\\\\n|[^\"])*\")" +
        "|[A-Z_a-z][A-Z_a-z0-9]*|==|<=|>=|&&|\\|\\||\\p{Punct})?"

const val NEXT = 0

const val factoryName = "create"

/**
 * Tokenオブジェクトより例外詳細を取得し、文字列に変換する関数
 * @param t Token 例外が発生した部分のToken
 * @return String 例外として表示する文字列
 */
fun location(t: Token): String {
    return if (t == EOF) {
        "the last line"
    } else {
        "\" ${t.getText()} \" at line ${t.lineNumber}"
    }
}

typealias Fun<T> = () -> T

fun test(func: Fun<String>) {
    val str = func()
    println(str)
}
