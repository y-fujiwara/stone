package jp.yfujiwara.stone

/**
 *
 * @property lineNumber Int
 * @constructor
 */
abstract class Token internal constructor(val lineNumber: Int) {
    open fun isIdentifier(): Boolean = false
    open fun isNumber(): Boolean = false
    open fun isString(): Boolean = false
    // ここもnewが省かれている
    open fun getNumber(): Int = throw StoneException("not number token")
    open fun getText(): String = ""
}