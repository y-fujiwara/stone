package jp.yfujiwara.stone

import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.Reader
import javax.swing.JFileChooser
import javax.swing.JOptionPane
import javax.swing.JScrollPane
import javax.swing.JTextArea

class CodeDialog : Reader() {
    private var buffer: String? = null
    private var pos = 0

    override fun read(cbuf: CharArray?, off: Int, len: Int): Int {
        if (buffer == null) {
            val readIn = showDialog()

            if (readIn == null) {
                return -1
            } else {
                println(readIn)
                buffer = "$readIn\n"
                pos = 0
            }
        }
        var size = 0
        // !!をつけることでString?からStringに変換している
        // この場合は上のif文でnullチェックするので問題ないはず
        val length = buffer!!.length

        // TODO: nullの扱い方がイマイチ. cbufのチェックは要らないのだろうか
        while (pos < length && size < len) {
            cbuf!![off + size++] = buffer!![pos++]
        }

        if (pos == length) {
            buffer = null
        }

        return size
    }

    override fun close() {}

    protected fun showDialog(): String? {
        val area = JTextArea(20, 40)
        val pane = JScrollPane(area)
        val result = JOptionPane.showOptionDialog(
            null,
            pane,
            "Input",
            JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.PLAIN_MESSAGE,
            null,
            null,
            null
        )

        return if (result == JOptionPane.OK_OPTION) {
            area.text
        } else {
            null
        }
    }

    companion object {
        fun file(): Reader {
            val chooser = JFileChooser()
            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                return BufferedReader(FileReader(chooser.selectedFile))
            } else {
                throw FileNotFoundException("no file specified")
            }
        }
    }
}