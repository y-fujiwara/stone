package jp.yfujiwara.stone

import java.lang.RuntimeException
import jp.yfujiwara.stone.ast.ASTree;

class StoneException(m: String) : RuntimeException(m) {
    constructor(m: String, t: ASTree) : this("$m ${t.location()}") {
    }
}