package jp.yfujiwara.stone.Interpreter

class BasicEnv: Environment {
    internal val values: HashMap<String, Any> = HashMap()

    override fun put(name: String, value: Any) {
        values[name] =  value
    }

    override fun get(name: String) = values[name]
}