package jp.yfujiwara.stone.Interpreter

/**
 * Interpreter上の環境表現
 */
interface Environment {
     fun put(name: String, value: Any)
     fun get(name: String): Any?
}