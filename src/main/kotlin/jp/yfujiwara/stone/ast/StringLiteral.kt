package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.Interpreter.Environment
import jp.yfujiwara.stone.Token

open class StringLiteral(t: Token): ASTLeaf(t)  {
    fun value() = token().getText()
    override fun eval(env: Environment): Any = value()
}