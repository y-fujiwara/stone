package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.Interpreter.Environment
import jp.yfujiwara.stone.Token

/**
 * 何かしらの数字を表すクラス
 * 数字の内容は親クラスに定義されるプロパティのtokenオブジェクトに従う
 * @constructor
 */
open class NumberLiteral(t: Token) : ASTLeaf(t) {
    fun value() = token().getNumber()
    override fun eval(env: Environment): Any = value()
}