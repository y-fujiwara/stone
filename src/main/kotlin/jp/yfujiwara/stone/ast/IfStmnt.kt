package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.FALSE
import jp.yfujiwara.stone.Interpreter.Environment

open class IfStmnt(c: List<ASTree>) : ASTList(c) {
    fun condition() = child(0)
    fun thenBlock() = child(1)
    fun elseBlock() = if (numChildren() > 2) {
        child(2)
    } else {
        null
    }

    override fun toString() = "(if ${condition()} ${thenBlock()} else ${elseBlock()})"

    override fun eval(env: Environment): Any {
        val c = condition().eval(env)
        return if (c is Int && c != FALSE) {
            thenBlock().eval(env)
        } else {
            val b = elseBlock()
            if (b == null) {
                0
            } else {
                b.eval(env)
            }
        }
    }
}