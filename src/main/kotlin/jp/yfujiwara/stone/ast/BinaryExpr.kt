package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.FALSE
import jp.yfujiwara.stone.Interpreter.Environment
import jp.yfujiwara.stone.StoneException
import jp.yfujiwara.stone.TRUE

/**
 * 何かしらの式を表すクラス<br />
 * 式の項それぞれをリストとして持つ
 * @constructor
 */
open class BinaryExpr(c: List<ASTree>) : ASTList(c) {
    /**
     * 左辺の要素を返すメソッド
     * 実際は二項演算を想定するため0番目の要素を返す
     * @return ASTree
     */
    fun left() = child(0)

    /**
     * 演算子を返すメソッド
     * 実際は二項演算を想定するため1番目の要素を返す
     * @return String
     */
    fun operator() = (child(1) as ASTLeaf).token().getText()

    /**
     * 右辺の要素を返すメソッド
     * 実際は二項演算を想定するため2番目の要素を返す
     * @return ASTree
     */
    fun right() = child(2)

    override fun eval(env: Environment): Any {
        val op = operator()
        return if ("=" == op) {
            val right = right().eval(env)
            computeAssign(env, right)
        } else {
            val left = left().eval(env)
            val right = right().eval(env)
            computeOp(left, op, right)
        }
    }

    internal fun computeAssign(env: Environment, rvalue: Any): Any {
        val l = left()
        if (l is Name) {
            env.put(l.name(), rvalue)
            return rvalue
        } else {
            throw StoneException("bas assignment", this)
        }
    }

    internal fun computeOp(left: Any?, op: String, right: Any?): Any {
        return if (left is Int && right is Int) {
            computeNumber(left, op, right)
        } else {
            return if (op == "+") {
                left.toString() + right.toString()
            } else if (op == "==") {
                if (left == null) {
                    if (right == null) TRUE else FALSE
                } else {
                    if (left == right) TRUE else FALSE
                }
            } else {
                throw StoneException("bad type", this)
            }
        }
    }

    internal fun computeNumber(left: Int, op: String, right: Int): Any {
        return when (op) {
            "+" -> left + right
            "-" -> left - right
            "*" -> left * right
            "/" -> left / right
            "%" -> left % right
            "==" -> if (left == right) TRUE else FALSE
            ">" -> if (left > right) TRUE else FALSE
            "<" -> if (left < right) TRUE else FALSE
            else -> throw StoneException("bad operator", this)
        }
    }
}