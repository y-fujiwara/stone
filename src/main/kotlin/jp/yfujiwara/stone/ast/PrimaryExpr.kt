package jp.yfujiwara.stone.ast

class PrimaryExpr(c: List<ASTree>) :ASTList(c) {
    companion object {
        /**
         * コンストラクタの代わりにインスタンスを生成するメソッド<br />
         * リフレクションの都合でJvmStaticにする
         * @param c List<ASTree>
         * @return ASTree
         */
        @JvmStatic
        fun create(c: List<ASTree>) = if (c.size == 1) {
            // 要素が残り一つの場合はListでの表現ではなく直接トークンを返す(要素一つのASTListが挟まって邪魔なので)
            c[0]
        } else {
            PrimaryExpr(c)
        }
    }
}