package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.Interpreter.Environment

/**
 * 葉、節のいずれかを表すための抽象クラス
 */
abstract class ASTree : Iterable<ASTree> {
    abstract fun location(): String?
    abstract fun child(i: Int): ASTree
    abstract fun numChildren(): Int
    abstract fun children(): Iterator<ASTree>
    abstract fun eval(env: Environment): Any
    override fun iterator(): Iterator<ASTree> = children()
}