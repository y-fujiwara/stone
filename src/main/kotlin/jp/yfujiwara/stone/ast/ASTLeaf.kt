package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.Interpreter.Environment
import jp.yfujiwara.stone.StoneException
import jp.yfujiwara.stone.Token
import java.lang.IndexOutOfBoundsException

/**
 * 抽象構文木の葉を表すクラス(子を持たない節)<br />
 * 具体的にはプロパティのtokenオブジェクトの内容に従う
 * @property token Token
 * @constructor
 */
open class ASTLeaf(protected val token: Token) : ASTree() {
    companion object {
        private val empty = emptyList<ASTree>()
    }

    override fun location(): String = "at line ${token.lineNumber}"

    override fun child(i: Int): ASTree {
        throw IndexOutOfBoundsException()
    }

    override fun numChildren(): Int = 0

    override fun children(): Iterator<ASTree> = empty.iterator()

    override fun toString() = token.getText()

    override fun eval(env: Environment): Any {
        throw StoneException("cannnot eval: ${toString()}", this)
    }

    fun token() = token
}