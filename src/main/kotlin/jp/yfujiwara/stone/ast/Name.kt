package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.Interpreter.Environment
import jp.yfujiwara.stone.StoneException
import jp.yfujiwara.stone.Token

/**
 * 何かしらの文字トークンを表すクラス
 * 文字の内容は親クラスに定義されるプロパティのtokenオブジェクトに従う
 * @constructor
 */
open class Name(t: Token) : ASTLeaf(t) {
    fun name() = token.getText()
    override fun eval(env: Environment): Any = env.get(name()) ?: throw StoneException("undefined name: ${name()}", this)
}