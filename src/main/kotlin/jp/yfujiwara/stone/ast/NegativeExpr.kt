package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.Interpreter.Environment
import jp.yfujiwara.stone.StoneException

open class NegativeExpr(c: List<ASTree>) : ASTList(c) {
    fun operand() = child(0)
    override fun toString() = "-${operand()}"
    override fun eval(env: Environment): Any {
        val v = operand().eval(env)
        if (v is Int) {
            return -v
        } else {
            throw StoneException("bad type for -", this)
        }
    }
}