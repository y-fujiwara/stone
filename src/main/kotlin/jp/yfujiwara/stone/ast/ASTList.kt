package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.Interpreter.Environment
import jp.yfujiwara.stone.StoneException
import java.lang.StringBuilder

/**
 * 抽象構文木の節を表すクラス(子を持つ節)
 * 今回は二分木ではないので子をリストとして保持する
 * @property children List<ASTree>
 * @constructor
 */
open class ASTList(protected var children: List<ASTree>) : ASTree() {

    override fun location(): String? {
        for (t in children) {
            val s = t.location()
            if (s != null) {
                return s
            }
        }
        return null
    }

    override fun child(i: Int) = children[i]
    override fun numChildren() = children.size
    override fun children() = children.iterator()

    /**
     * 抽象構文木を元の式文字列に戻すようなtoStringメソッド
     * @return String
     */
    override fun toString(): String {
        val builder = StringBuilder()
        builder.append('(')
        var sep = ""
        for (t in children) {
            builder.append(sep)
            sep = " "
            builder.append(t.toString())
        }
        return builder.append(')').toString()
    }

    override fun eval(env: Environment): Any {
        throw StoneException("cannnot eval: ${toString()}", this)
    }
}