package jp.yfujiwara.stone.ast

import jp.yfujiwara.stone.FALSE
import jp.yfujiwara.stone.Interpreter.Environment

open class WhileStmnt(c: List<ASTree>) : ASTList(c) {
    fun condition() = child(0)
    fun body() = child(1)
    override fun toString() = "(while ${condition()} ${body()})"
    override fun eval(env: Environment): Any {
        var result: Any = 0
        while (true) {
            val c = condition().eval(env)
            if (c is Int && c == FALSE) {
                return result
            } else {
                result = body().eval(env)
            }
        }
    }
}