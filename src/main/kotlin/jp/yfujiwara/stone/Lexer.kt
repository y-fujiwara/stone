package jp.yfujiwara.stone

import java.io.IOException
import java.io.LineNumberReader
import java.io.Reader
import java.util.regex.Matcher
import java.util.regex.Pattern

open class Lexer(r: Reader) {
    private val pattern = Pattern.compile(regexPat)
    private var queue = mutableListOf<Token>()
    private var hasMore: Boolean = true
    private var reader: LineNumberReader = LineNumberReader(r)

    /**
     * 対象ソースコードからトークンを一つ取り出すメソッド
     * @return Token 取り出されたトークン。なければEOF
     */
    fun read(): Token {
        return if (fillQueue(0)) {
            queue.removeAt(0)
        } else {
            EOF
        }
    }

    /**
     * 対象のソースコードからi番目のトークンを先読みするメソッド
     * @param i Int 読み込むトークンの番号
     * @return Token 読み込まれたトークン。なければEOF
     */
    fun peek(i: Int): Token {
        return if (fillQueue(i))
            queue[i]
        else
            EOF
    }

    private fun fillQueue(i: Int): Boolean {
        while (i >= queue.size) {
            if (hasMore) {
                readLine()
            } else {
                return false
            }
        }
        return true
    }

    /**
     * 一行読み込んで正規表現にマッチするか検査するメソッド
     * マッチした場合はメンバ変数のqueueに詰め込まれる
     */
    protected fun readLine() {
        // nullが考えられる変数には?をつけると強制的にnullチェックさせられる
        val line: String?
        try {
            line = reader.readLine()
        } catch (e: IOException) {
            throw ParseException(e)
        }

        if (line == null) {
            hasMore = false
            return
        }

        val lineNo = reader.lineNumber
        val matcher = pattern.matcher(line)

        // useTransparentBoundsはregionを超えてもその前後が検査できるかどうかの設定(true)
        // useAnchoringBoundsはregionを設定しても正規表現マッチ行の先頭の^や末尾$はそのままのチェックにできる設定(false)
        matcher.useTransparentBounds(true).useAnchoringBounds(false)

        var pos = 0
        val endPos = line.length
        while (pos < endPos) {
            // posからendPosで正規表現にマッチするか検査
            matcher.region(pos, endPos)
            // 正規表現マッチを開始するlookingAtを発火
            if (matcher.lookingAt()) {
                addToken(lineNo, matcher)
                pos = matcher.end()
            } else {
                throw ParseException("bad token at line $lineNo")
            }
        }
        queue.add(IdToken(lineNo, EOL))
    }

    protected fun addToken(lineNo: Int, matcher: Matcher) {
        val m = matcher.group(1)
        // マッチした文字列が空白文字列ではない
        if (m != null) {
            // マッチした文字列がコメントではない = コメントの部分がnull
            if (matcher.group(2) == null) {
                val token: Token = when {
                    // toIntはKotlinの拡張メソッド
                    matcher.group(3) != null ->
                        NumToken(lineNo, m.toInt())
                    matcher.group(4) != null -> StrToken(lineNo, toStringLiteral(m))
                    else -> IdToken(lineNo, m)
                }
                queue.add(token)
            }
        }
    }

    /**
     * 正規表現が文字列リテラルにマッチしたときに対象の文字列を取得するメソッド
     * @param s String
     * @return String
     */
    protected fun toStringLiteral(s: String): String {
        val sb = StringBuilder()
        val len = s.length - 1
        var i = 1
        // 普通のfor文でindexを変更するのが難しい？のでwhileで代用
        // そもそもstringクラスが持つforEach系メソッドでなんとかしたいところ
        while (i < len) {
            i += 1
            var c = s[i]
            if (c == '\\' && (i + 1) < len) {
                val c2 = s[i + 1]
                if (c2 == '"' || c2 == '\\') {
                    c = s[++i]
                } else if (c2 == 'n') {
                    ++i
                    c = '\n'
                }
            }
            sb.append(c)
        }
        return sb.toString()
    }

    // protected static classesの再現
    companion object {
        protected class NumToken internal constructor(line: Int, private val value: Int) : Token(line) {
            override fun isNumber() = true
            override fun getText() = value.toString()
            override fun getNumber() = value
        }

        protected class IdToken internal constructor(line: Int, private val text: String) : Token(line) {
            override fun isIdentifier() = true
            override fun getText() = text
        }

        protected class StrToken(line: Int, private val literal: String) : Token(line) {
            override fun isString() = true
            override fun getText() = literal
        }
    }
}
