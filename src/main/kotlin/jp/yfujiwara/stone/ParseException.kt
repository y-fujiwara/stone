package jp.yfujiwara.stone

import java.io.IOException
import java.lang.Exception

/**
 * 字句解析字のパースエラークラス
 */
class ParseException : Exception {

    /**
     * トークンの処理中にエラーが起きた場合のコンストラクタ
     * トークン情報を文字列に変換するコンストラクタを呼び出す
     * @param t Token 対象トークン
     * @constructor
     */
    constructor(t: Token) : this("", t) {
    }

    /**
     * トークンから対象のトークン文字列を行番号をエラーとして吐き出すコンストラクタ
     * @param msg String プレフィックス文字列
     * @param t Token 例外発生時のトークン
     * @constructor
     */
    constructor(msg: String, t: Token) : super("syntax error around ${location(t)} . $msg") {
    }

    /**
     * その他IO処理中に発生した例外を処理するコンストラクタ
     * @param e IOException
     * @constructor
     */
    constructor(e: IOException) : super(e) {
    }

    /**
     * 任意の例外を発生させられた時用のコンストラクタ
     * @param msg String 例外詳細文字列
     * @constructor
     */
    constructor(msg: String) : super(msg) {
    }
}