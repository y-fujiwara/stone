package chap3

import jp.yfujiwara.stone.CodeDialog
import jp.yfujiwara.stone.EOF
import jp.yfujiwara.stone.Lexer

fun main(args : Array<String>) {
    val l = Lexer(CodeDialog())
    while (true) {
        val t = l.read()
        if (t == EOF) {
            return
        } else {
            println("=> ${t.getText()}")
        }
    }
}

