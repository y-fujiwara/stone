package chap5

import jp.yfujiwara.stone.*


fun main(args: Array<String>) {
    val l = Lexer(CodeDialog())
    val bp = BasicParser()

    while (l.peek(NEXT) != EOF) {
        val ast = bp.parse(l)
        println("=> ${ast.toString()}")
    }
}

