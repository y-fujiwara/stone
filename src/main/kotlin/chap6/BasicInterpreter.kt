package chap6

import jp.yfujiwara.stone.BasicParser
import jp.yfujiwara.stone.CodeDialog
import jp.yfujiwara.stone.EOF
import jp.yfujiwara.stone.Interpreter.BasicEnv
import jp.yfujiwara.stone.Interpreter.Environment
import jp.yfujiwara.stone.Lexer
import jp.yfujiwara.stone.ast.NullStmnt


class BasicInterpreter {
    companion object {
        @JvmStatic
        fun run(bp: BasicParser, env: Environment) {
            val lexer = Lexer(CodeDialog())
            while (lexer.peek(0) != EOF) {
                val t = bp.parse(lexer)
                if (t !is NullStmnt) {
                    val r = t.eval(env)
                    println("=> $r")
                }
            }
        }

        @JvmStatic
        fun main(args: Array<String>) {
            run(BasicParser(), BasicEnv())
        }
    }
}
