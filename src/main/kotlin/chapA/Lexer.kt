package chapA

import java.io.IOException
import java.io.Reader
import jp.yfujiwara.stone.CodeDialog

const val EMPTY = -1

const val A = 'A'.toInt()
const val Z = 'Z'.toInt()
const val a = 'a'.toInt()
const val z = 'z'.toInt()
const val SPACE = ' '.toInt()
const val ZERO = '0'.toInt()
const val NINE = '9'.toInt()
const val EQUAL = '='.toInt()

class Lexer(private val reader: Reader) {

    /**
     * 最後に読み込んだ文字を保持する
     */
    private var lastChar = EMPTY

    /**
     * Readerから一文字読み込むメソッド
     * 読み込みがなかったことにされた場合には同じ文字を返す
     * @return Int
     */
    private fun getChar(): Int {
        return if (lastChar == EMPTY) {
            reader.read()
        } else {
            val c = lastChar
            lastChar = EMPTY
            c
        }
    }

    /**
     * 一度読んだ文字を読まなかったことにする
     * @param c Int
     */
    private fun ungetChar(c: Int) {
        lastChar = c
    }

    fun read(): String? {
        val sb = StringBuilder()
        var c: Int

        do {
            // スペースをすべて読み込む
            c = getChar()
        } while (isSpace(c))

        when {
            c < 0 -> return null
            isDigit(c) ->
                // 数字じゃないものが出てくるまで読み込み続ける
                // 123みたいな数字だけのものを判別
                do {
                    sb.append(c.toChar())
                    c = getChar()
                } while (isDigit(c))
            isLetter(c) ->
                do {
                    // 文字じゃないものが出てくるまで読み込み続ける
                    sb.append(c.toChar())
                    c = getChar()
                    // "a123みたいなものが考えられるので判定はどちらかに入れば良い
                } while (isLetter(c) || isDigit(c))
            c == EQUAL -> {
                // =が読み込まれた場合はひとつ先も先読みして==か判別する
                c = getChar()
                return if (c == EQUAL) {
                    "=="
                } else {
                    ungetChar(c)
                    "="
                }
            }
            else -> throw IOException()
        }

        if (c >= 0) {
            ungetChar(c)
        }
        return sb.toString()
    }

    companion object {
        private fun isLetter(c: Int) = c in A..Z || c in a..z
        private fun isDigit(c: Int) = c in ZERO..NINE
        private fun isSpace(c: Int) = c in 0..SPACE
    }
}

fun main(args: Array<String>) {
    val l = Lexer(CodeDialog())
    while (true) {
        val s = l.read()
        if (s == null) {
            return
        } else {
            println("-> $s")
        }
    }
}
