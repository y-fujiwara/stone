package chapB

import jp.yfujiwara.stone.CodeDialog
import jp.yfujiwara.stone.Lexer
import jp.yfujiwara.stone.ParseException
import jp.yfujiwara.stone.ast.ASTLeaf
import jp.yfujiwara.stone.ast.ASTree
import jp.yfujiwara.stone.ast.BinaryExpr
import jp.yfujiwara.stone.ast.NumberLiteral

class OpPrecedenceParser(p: Lexer) {
    private val lexer: Lexer = p
    protected val operators: HashMap<String, Precedence> = HashMap()

    companion object {
        class Precedence(val value: Int, val leftAssoc: Boolean) {}

        fun rightIsExpr(prec: Int, nextPrec: Precedence): Boolean {
            return if (nextPrec.leftAssoc) {
                prec < nextPrec.value
            } else {
                prec <= nextPrec.value
            }
        }
    }

    init {
        operators["<"] = Precedence(1, true)
        operators[">"] = Precedence(1, true)
        operators["+"] = Precedence(2, true)
        operators["-"] = Precedence(2, true)
        operators["*"] = Precedence(3, true)
        operators["/"] = Precedence(3, true)
        operators["^"] = Precedence(4, false)
    }

    fun expression(): ASTree {
        var right = factor()
        var next: Precedence? = nextOperator()

        while (next != null) {
            right = doShift(right, next.value)
            next = nextOperator()
        }
        return right
    }

    /**
     * BNFの非終端記号factorを表すメソッド
     * factor: NUMBER | "(" expression ")"
     */
    private fun factor(): ASTree {
        // 先読みした結果が(ならBNFのorの右側
        return if (isToken("(")) {
            token("(")
            // 相互再帰
            // カッコの中身はexpression
            val e = expression()
            token(")")
            e
        } else {
            // カッコじゃなければただの数字
            val t = lexer.read()
            if (t.isNumber()) {
                val n = NumberLiteral(t)
                n
            } else {
                throw ParseException(t)
            }
        }
    }

    private fun token(name: String) {
        val t = lexer.read()
        if (!(t.isIdentifier() && name == t.getText())) {
            throw ParseException(t)
        }
    }

    private fun isToken(name: String): Boolean {
        val t = lexer.peek(NEXT)
        return t.isIdentifier() && name == t.getText()
    }

    private fun doShift(left: ASTree, prec: Int): ASTree {
        val op = ASTLeaf(lexer.read())
        var right = factor()
        var next: Precedence? = nextOperator()

        while (next != null && rightIsExpr(prec, next)) {
            right = doShift(right, next.value)
            next = nextOperator()
        }
        return BinaryExpr(listOf(left, op, right))
    }

    private fun nextOperator(): Precedence? {
        val t = lexer.peek(NEXT)
        if (t.isIdentifier()) {
            return operators[t.getText()]
        } else {
            return null
        }
    }
}

fun main(args: Array<String>) {
    val lexer = Lexer(CodeDialog())
    val p = OpPrecedenceParser(lexer)
    val t = p.expression()
    System.out.println("=> $t")
}