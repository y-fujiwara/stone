package chapB

import jp.yfujiwara.stone.CodeDialog
import jp.yfujiwara.stone.Lexer
import jp.yfujiwara.stone.ParseException
import jp.yfujiwara.stone.ast.ASTLeaf
import jp.yfujiwara.stone.ast.ASTree
import jp.yfujiwara.stone.ast.BinaryExpr
import jp.yfujiwara.stone.ast.NumberLiteral

const val NEXT = 0

/**
 * LL(1)構文解析による四則演算構文解析
 */
public class ExprParser(private val lexer: Lexer) {

    /**
     * BNFの非終端記号expressionを表すメソッド
     * expression: term { ("+" | "-") term }
     */
    fun expression(): ASTree {
        // BNFの先頭termを読み込む
        var left = term()
        // 一つ先読みして+か-が来ないならtermで終わっているで終了(剰余だけか数字のみ)
        while (isToken("+") || isToken("-")) {
            val op = ASTLeaf(lexer.read())
            // 相互再帰
            val right = term()
            left = BinaryExpr(listOf(left, op, right))
        }
        return left
    }

    /**
     * BNFの非終端記号termを表すメソッド
     * term: factor { ("*" | "/") factor }
     */
    private fun term(): ASTree {
        // BNFの先頭factorを読み込む。factorは数字かexpressionになる
        var left = factor()
        // 一つ先読みして*か/が来ないならfactorで終わっているので終了(数字か和減算のみ)
        while (isToken("*") || isToken("/")) {
            val op = ASTLeaf(lexer.read())
            // 相互再帰
            val right = factor()
            left = BinaryExpr(listOf(left, op, right))
        }
        return left
    }

    /**
     * BNFの非終端記号factorを表すメソッド
     * factor: NUMBER | "(" expression ")"
     */
    private fun factor(): ASTree {
        // 先読みした結果が(ならBNFのorの右側
        return if (isToken("(")) {
            token("(")
            // 相互再帰
            // カッコの中身はexpression
            val e = expression()
            token(")")
            e
        } else {
            // カッコじゃなければただの数字
            val t = lexer.read()
            if (t.isNumber()) {
                val n = NumberLiteral(t)
                n
            } else {
                throw ParseException(t)
            }
        }
    }

    private fun token(name: String) {
        val t = lexer.read()
        if (!(t.isIdentifier() && name == t.getText())) {
            throw ParseException(t)
        }
    }

    private fun isToken(name: String): Boolean {
        val t = lexer.peek(NEXT)
        return t.isIdentifier() && name == t.getText()
    }

}

fun main(args: Array<String>) {
    val lexer = Lexer(CodeDialog())
    val p = ExprParser(lexer)
    val t = p.expression()
    println("=> $t")
}